WITH subcategory_sales AS (
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) AS sales_year,
        SUM(s.amount_sold) AS total_sales
    FROM
        sh.sales s
    JOIN
        sh.products p ON s.prod_id = p.prod_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, sales_year
)

, previous_year_sales AS (
    SELECT
        prod_subcategory,
        sales_year,
        LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY sales_year) AS previous_year_sales
    FROM
        subcategory_sales
)

, comparison AS (
    SELECT
        s.prod_subcategory,
        s.sales_year,
        s.total_sales,
        p.previous_year_sales
    FROM
        subcategory_sales s
    JOIN
        previous_year_sales p ON s.prod_subcategory = p.prod_subcategory AND s.sales_year = p.sales_year
)

SELECT DISTINCT
    prod_subcategory
FROM
    comparison
WHERE
    total_sales > previous_year_sales;