SELECT customer_id, customer_first_name, customer_last_name, channel_id, total_amount, customer_rank
FROM (
    SELECT 
        c.cust_id AS customer_id,
        c.cust_first_name AS customer_first_name,
        c.cust_last_name AS customer_last_name,
        ch.channel_id,
        round(SUM(s.amount_sold), 2) AS total_amount,
        RANK() OVER (PARTITION BY ch.channel_id ORDER BY round(SUM(s.amount_sold), 2) DESC) AS customer_rank 
    FROM
        sh.customers c 
        JOIN sh.sales s ON c.cust_id = s.cust_id
        JOIN sh.channels ch ON s.channel_id = ch.channel_id  
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2000, 2001)
    GROUP BY
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        ch.channel_id
) ranked
WHERE customer_rank  <= 300;